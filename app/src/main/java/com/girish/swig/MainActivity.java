package com.girish.swig;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.girish.swig.data.Restaurant;
import com.girish.swig.data.RestaurantData;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Restaurant> mRestList;
    private MyAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private boolean mIsLoading = false;
    private int mNextPage = 0;

    private Observer<RestaurantData> mObserver = new Observer<RestaurantData>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mIsLoading = false;
            showToast("something went wrong");
        }

        @Override
        public void onNext(RestaurantData data) {
            // increment the page after a successful call.
            mNextPage += 1;
            onRestaurantFetched(data.restaurants);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RetrofitClient.init();
        init();
    }

    private void init() {

        mRestList = new ArrayList<>();
        mAdapter = new MyAdapter(mRestList);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = mLayoutManager.getItemCount();
                int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (!mIsLoading && totalItemCount == (lastVisibleItem + 1)) {
                    onLoadMore();
                }
            }
        });
        getRestaurants(mNextPage);
    }

    private void getRestaurants(int page) {
        if (!mIsLoading) {
            mIsLoading = true;
            mRestList.add(null);
            mAdapter.notifyItemInserted(mRestList.size() - 1);
            RestaurantService service = RetrofitClient.get().create(RestaurantService.class);
            Observable<RestaurantData> observable = service.getRestaurants(page)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
            observable.subscribe(mObserver);
        }
    }

    private void onLoadMore() {
        getRestaurants(mNextPage);
    }

    private void onRestaurantFetched(ArrayList<Restaurant> restaurants) {
        mIsLoading = false;
        mRestList.remove(mRestList.size() - 1);
        mAdapter.notifyItemRemoved(mRestList.size());
        mRestList.addAll(restaurants);
        mAdapter.notifyDataSetChanged();
    }

    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT);
    }
}
