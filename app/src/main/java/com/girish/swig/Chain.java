package com.girish.swig;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Chain {

    @SerializedName("name")
    String name;

    @SerializedName("city")
    String city;

    @SerializedName("area")
    String area;

    @SerializedName("avg_rating")
    String avg_rating;

    @SerializedName("cid")
    String cid;

    @SerializedName("cuisine")
    ArrayList<String> cuisine;

    @SerializedName("closed")
    boolean closed;

    @SerializedName("deliveryTime")
    String deliveryTime;

    @SerializedName("costForTwo")
    String costForTwo;
}
