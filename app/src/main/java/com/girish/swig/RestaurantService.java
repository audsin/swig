package com.girish.swig;

import com.girish.swig.data.Restaurant;
import com.girish.swig.data.RestaurantData;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

public interface RestaurantService {

    @GET("/bins/ngcc")
    Observable<RestaurantData> getRestaurants(@Query("page") int page);
}
