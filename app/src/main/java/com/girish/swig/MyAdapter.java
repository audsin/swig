package com.girish.swig;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.girish.swig.data.Restaurant;

import java.util.ArrayList;

/**
 * Created by fcgb17800 on 03/11/16.
 */

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_ITEM = 0;
    private final int TYPE_LOADING = 1;

    private final ArrayList<Restaurant> mRestList;

    public MyAdapter(ArrayList<Restaurant> restList) {
        mRestList = restList;
    }

    @Override
    public int getItemViewType(int position) {
        return mRestList.get(position) != null ? TYPE_ITEM : TYPE_LOADING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        if (viewType == TYPE_ITEM) {
            view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.restaurant_list_item, parent, false);
            return new ItemViewHolder(view);
        } else {
            view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.loading_item, parent, false);
            return new LoadingViewHolder(view);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ItemViewHolder) {
            final ItemViewHolder holder = (ItemViewHolder) viewHolder;
            final Restaurant restaurant = mRestList.get(position);
            holder.mName.setText(restaurant.name);
            holder.mCuisines.setText(restaurant.cuisine.get(0));
            holder.mRating.setText(restaurant.avg_rating);
            holder.mCost.setText(restaurant.costForTwo);
            holder.mTime.setText(restaurant.deliveryTime + "min");
            holder.mLayout.removeAllViews();

            Glide.with(holder.itemView.getContext())
                    .load(Const.img_url + restaurant.cid)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .into(holder.mImage);

            if (restaurant.chains != null) {
                holder.mOutlet.setVisibility(View.VISIBLE);
                holder.mOutlet.setText(restaurant.chains.size() + " outlets around you");
                holder.mIsOpen = false;

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!holder.mIsOpen) {
                            expandView(holder, restaurant);
                        } else {
                            collapseView(holder, restaurant);
                        }
                        holder.mIsOpen = !holder.mIsOpen;
                    }
                });
            } else {
                holder.mOutlet.setVisibility(View.GONE);
            }
        }

    }

    private void collapseView(ItemViewHolder holder, Restaurant restaurant) {
        LinearLayout layout = holder.mLayout;
        for (int i = 0; i < restaurant.chains.size(); i++) {
            layout.removeViewAt(layout.getChildCount() - 1);
        }
    }

    private void expandView(ItemViewHolder holder, Restaurant restaurant) {
        LinearLayout layout = holder.mLayout;
        View view;

        for (Chain chain : restaurant.chains) {
            view = LayoutInflater.from(layout.getContext()).inflate(R.layout.chain_layout, layout, false);
            ((TextView) view.findViewById(R.id.chain_address)).setText(chain.area);
            ((TextView) view.findViewById(R.id.chain_rating)).setText(chain.avg_rating);
            ((TextView) view.findViewById(R.id.chain_time)).setText(chain.deliveryTime + " min");
            layout.addView(view);
        }

    }

    @Override
    public int getItemCount() {
        return mRestList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView mName;
        TextView mCuisines;
        ImageView mImage;
        TextView mCost;
        TextView mRating;
        TextView mTime;
        TextView mOutlet;
        LinearLayout mLayout;
        boolean mIsOpen = false;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.name);
            mCuisines = (TextView) itemView.findViewById(R.id.cuisines);
            mImage = (ImageView) itemView.findViewById(R.id.rest_image);
            mCost = (TextView) itemView.findViewById(R.id.cost);
            mRating = (TextView) itemView.findViewById(R.id.rating);
            mTime = (TextView) itemView.findViewById(R.id.delivery_time);
            mOutlet = (TextView) itemView.findViewById(R.id.outlets);
            mLayout = (LinearLayout) itemView.findViewById(R.id.chain_container);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(View view) {
            super(view);
        }
    }
}
