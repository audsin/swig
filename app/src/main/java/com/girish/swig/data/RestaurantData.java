package com.girish.swig.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestaurantData {

    @SerializedName("restaurants")
    public ArrayList<Restaurant> restaurants;
}
