package com.girish.swig.data;

import com.girish.swig.Chain;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {

    @SerializedName("name")
    public String name;

    @SerializedName("city")
    public String city;

    @SerializedName("area")
    public String area;

    @SerializedName("avg_rating")
    public String avg_rating;

    @SerializedName("cid")
    public String cid;

    @SerializedName("cuisine")
    public ArrayList<String> cuisine;

    @SerializedName("chain")
    public ArrayList<Chain> chains;

    @SerializedName("closed")
    public boolean closed;

    @SerializedName("deliveryTime")
    public String deliveryTime;

    @SerializedName("costForTwo")
    public String costForTwo;
}
